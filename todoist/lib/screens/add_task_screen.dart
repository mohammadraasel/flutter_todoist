import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoist/models/task_data.dart';

class AddTaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String newTaskTitle;
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 35),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Add Task',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              TextField(
                onChanged: (value) {
                  newTaskTitle = value;
                },
                cursorColor: Color(0xFFDB4C3F),
                decoration: InputDecoration(
                  hintText: 'Type here...',
                  suffixIcon: Icon(
                    Icons.create,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFDB4C3F),
                      width: 2,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              FlatButton(
                onPressed: () {
                  if (newTaskTitle == null || newTaskTitle == '') {
                    return;
                  }
                  Provider.of<TaskData>(context, listen: false).addTask(newTaskTitle);
                  Navigator.pop(context);
                },
                padding: EdgeInsets.symmetric(vertical: 10),
                color: Color(0xFFDB4C3F),
                child: Text(
                  'Add',
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
