import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoist/models/task_data.dart';
import 'package:todoist/screens/tasks_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context){
        return TaskData();
      },
      child: MaterialApp(
        theme: ThemeData.light().copyWith(
          primaryColor: Color(0xFFDB4C3F),
        ),
        debugShowCheckedModeBanner: false,
        home: TasksScreen(),
      ),
    );
  }
}

