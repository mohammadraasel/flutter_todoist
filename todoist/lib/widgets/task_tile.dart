import 'package:flutter/material.dart';
import 'package:todoist/constants/colors.dart';
import 'package:todoist/models/task.dart';

class TaskTile extends StatelessWidget {
  final Task task;
  final Function toggleTaskCompletion;
  final Function deleteTask;
  TaskTile({this.task, this.toggleTaskCompletion, this.deleteTask});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: deleteTask,
      contentPadding: EdgeInsets.symmetric(horizontal: 0,),
      title: Text(
        task.title,
        style: TextStyle(
            decoration: task.isComplete ? TextDecoration.lineThrough : null),
      ),
      trailing: Checkbox(
        value: task.isComplete,
        activeColor: kMainColor,
        onChanged: toggleTaskCompletion,
      ),
    );
  }
}


