import 'package:flutter/material.dart';
import 'package:todoist/widgets/task_tile.dart';
import 'package:todoist/models/task_data.dart';
import 'package:provider/provider.dart';

class TasksList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskData>(
      builder: (BuildContext context, taskData, child){
        return ListView.builder(
          itemCount: taskData.taskCount,
          itemBuilder: (BuildContext context, int index) {
            final task = taskData.tasks[index];
            return Padding(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: TaskTile(
                task: task,
                toggleTaskCompletion: (checkboxState) {
                  taskData.updateTask(task);
                },
                deleteTask: (){
                  taskData.deleteTask(task);
                },
              ),
            );
          },
        );
      },
    );
  }
}
