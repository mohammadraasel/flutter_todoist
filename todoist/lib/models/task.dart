class Task{
  String title;
  bool isComplete;

  Task({this.isComplete = false, this.title});

  void toggleTaskCompletion(){
    isComplete = !isComplete;
  }
}

